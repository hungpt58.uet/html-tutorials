# Learning

1. `::after` & `::before`

   - `::after` and `::before` are called pseudo-elements.
   - `::before` always create a pseudo-element is first child of root element.
   - `::after` always create a pseudo-element is last child of root element.
   - Can replace into `:after` & `:before`
   - Always need `content:` when using pseudo-elements
   - Display with `inline` style
   - Cannot use pseudo-elements for not-styleable elements ex: video, img, svg ...
   
2. `box-shadow`

   - `box-shadow: 6px 5px` -> 6px horizontal and 5px vertical. 
   - `box-shadow: 5px 5px 10px` -> 10px is for blur effect 
   - `box-shadow: 5px 5px 10px 9px` -> 9px is for spread effect 
   - Can set color for shadow
   - Using `inset` to make shadow inside. Default is shadow outside

3. `transform`

   - Using for 2D, 3D transformation
   - This property allows you to rotate, scale, move, skew, etc., elements.
   - `transform: rotate(20deg)` or `transform: skewY(20deg)` or `transform: scaleY(1.5)` or `transform: skewX(1.5)` or `transform: translate(10px, 10px)` ...

4. `inset`

   - Using for display: `relative`, `absolute`, `fixed`
   - Is shorthand for `top, right, bottom, left`
   - Ex: `inset: 4px` == `top: 4px; right: 4px; bottom: 4px; left: 4px;`

# Colors

- Link: https://mycolor.space/
- linear-gradient(120deg, #3e32a8, #e310c7)
- 